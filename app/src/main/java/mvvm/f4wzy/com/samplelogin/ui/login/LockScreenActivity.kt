package mvvm.f4wzy.com.samplelogin.ui.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_lockscreen.*
import mvvm.f4wzy.com.samplelogin.R
import mvvm.f4wzy.com.samplelogin.ui.login.viewmodel.LockScreenViewModel
import mvvm.f4wzy.com.samplelogin.util.BiometricPromptUtils

class LockScreenActivity :AppCompatActivity(), View.OnClickListener {
    var viewmodel: LockScreenViewModel? = null
    var Count = 0

    var biometricManager: BiometricManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_lockscreen)
        viewmodel = ViewModelProviders.of(this).get(LockScreenViewModel::class.java)
        biometricManager = BiometricManager.from(this)
        initView()


    }

    private fun initView() {

        btn0.setOnClickListener(this)
        btn1.setOnClickListener(this)
        btn2.setOnClickListener(this)
        btn3.setOnClickListener(this)
        btn4.setOnClickListener(this)
        btn5.setOnClickListener(this)
        btn6.setOnClickListener(this)
        btn7.setOnClickListener(this)
        btn8.setOnClickListener(this)
        btn9.setOnClickListener(this)
        delete_btn.setOnClickListener(this)
        fingerprint_btn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {

        when (v?.id){

            R.id.btn0 ->{

                addPin()

            }
            R.id.btn1 ->{

                addPin()

            }
            R.id.btn2 ->{

                addPin()

            }
            R.id.btn3 ->{

                addPin()

            }
            R.id.btn4 ->{

                addPin()

            }
            R.id.btn5 ->{

                addPin()

            }
            R.id.btn6 ->{

                addPin()

            }
            R.id.btn7 ->{

                addPin()

            }
            R.id.btn8 ->{

                addPin()

            }
            R.id.btn9->{

                addPin()

            }
            R.id.delete_btn ->{

                deletepin()

            }

            R.id.fingerprint_btn->{

                when (biometricManager?.canAuthenticate()) {
                    BiometricManager.BIOMETRIC_SUCCESS ->
                        showBiometricPrompt()
                    BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                        Toast.makeText(
                                this,
                                getString(R.string.error_msg_no_biometric_hardware),
                                Toast.LENGTH_LONG
                        ).show()
                    BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                        Toast.makeText(
                                this,
                                getString(R.string.error_msg_biometric_hw_unavailable),
                                Toast.LENGTH_LONG
                        ).show()
                    BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED ->
                        Toast.makeText(
                                this,
                                getString(R.string.error_msg_biometric_not_setup),
                                Toast.LENGTH_LONG
                        ).show()
                }


            }


        }


    }

    private fun showBiometricPrompt() {
        val biometricPromptUtils = BiometricPromptUtils(this, object : BiometricPromptUtils.BiometricListener {
            override fun onAuthenticationLockoutError() {

            }

            override fun onAuthenticationPermanentLockoutError() {
                // implement your permanent lockout error UI prompt
            }

            override fun onAuthenticationSuccess() {
                // implement your authentication success UI prompt

                ToActivity()
            }

            override fun onAuthenticationFailed() {
//                Toast.makeText(applicationContext,
//                        getString(R.string.error_msg_auth_failed),
//                        Toast.LENGTH_SHORT
//                ).show()
            }

            override fun onAuthenticationError() {
//                Toast.makeText(
//                        applicationContext,
//                        getString(R.string.error_msg_auth_error),
//                        Toast.LENGTH_SHORT
//                ).show()
            }
        })
        biometricPromptUtils.showBiometricPrompt(
                resources.getString(R.string.unlockWithBiometricsTitleKey),
                resources.getString(R.string.unlockWithBiometricsSubtitleKey),
                resources.getString(R.string.cancelKey),
                confirmationRequired = false
        )
    }

    private fun addPin() {

        when (Count){

            0 -> {
                cardview1?.setShadowColorDark(Color.BLACK)

                Count++
            }

            1 -> {
                cardview2?.setShadowColorDark(Color.BLACK)
                Count++
            }
            2 -> {
                cardview3?.setShadowColorDark(Color.BLACK)
                Count++
            }
            3 -> {
                cardview4?.setShadowColorDark(Color.BLACK)
                Count++
            }
            4 -> {
                cardview5?.setShadowColorDark(Color.BLACK)
                Count++
            }
            5 -> {
                cardview6?.setShadowColorDark(Color.BLACK)
               ToActivity()
            }


        }



    }

    private fun deletepin(){

        when (Count) {

            1 -> {
                cardview1?.setShadowColorDark(Color.LTGRAY)
                Count = 0
            }

            2 -> {
                cardview2?.setShadowColorDark(Color.LTGRAY)
                Count--
            }
            3 -> {
                cardview3?.setShadowColorDark(Color.LTGRAY)
                Count--
            }
            4 -> {
                cardview4?.setShadowColorDark(Color.LTGRAY)
                Count--
            }
            5 -> {
                cardview5?.setShadowColorDark(Color.LTGRAY)
                Count--
            }

        }
    }


    fun ToActivity(){

        //  Toast.makeText(getApplication(), "welcome", Toast.LENGTH_LONG).show()

        val intent = Intent(this, animationActivity::class.java)
        startActivity(intent)
        finish()

    }



}