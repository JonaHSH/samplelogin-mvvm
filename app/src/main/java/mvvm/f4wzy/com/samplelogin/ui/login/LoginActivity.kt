package mvvm.f4wzy.com.samplelogin.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import mvvm.f4wzy.com.samplelogin.R
import mvvm.f4wzy.com.samplelogin.databinding.ActivityLoginBinding
import mvvm.f4wzy.com.samplelogin.ui.login.viewmodel.LoginViewModel
import mvvm.f4wzy.com.samplelogin.util.CustomeProgressDialog

class LoginActivity : AppCompatActivity() {

    var binding: ActivityLoginBinding? = null
    var viewmodel: LoginViewModel? = null
    var customProgressDialog: CustomeProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewmodel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding?.viewmodel = viewmodel
        customProgressDialog = CustomeProgressDialog(this)
        initObservables()


    }

    private fun initObservables() {
        viewmodel?.progressDialog?.observe(this, Observer {
            if (it!!) customProgressDialog?.show() else customProgressDialog?.dismiss()
        })

        viewmodel?.userLogin?.observe(this, Observer { user ->
            Toast.makeText(this, "welcome, ${user?.username}", Toast.LENGTH_LONG).show()
            val intent = Intent(this, LockScreenActivity::class.java)
            startActivity(intent)
            finish()
        })
    }


}
