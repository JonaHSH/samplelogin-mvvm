package mvvm.f4wzy.com.samplelogin.network

import mvvm.f4wzy.com.samplelogin.model.User
import retrofit2.Call
import retrofit2.http.*

interface BackEndApi {

    @Headers("Content-Type: application/json; charset=UTF-8")
    @POST("posts")
    fun LOGIN(@Body UserData: User): Call<User>

}

