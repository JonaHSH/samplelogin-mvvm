package mvvm.f4wzy.com.samplelogin.ui.login.viewmodel

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import mvvm.f4wzy.com.samplelogin.model.User
import mvvm.f4wzy.com.samplelogin.network.BackEndApi
import mvvm.f4wzy.com.samplelogin.network.WebServiceClient
import mvvm.f4wzy.com.samplelogin.util.SingleLiveEvent
import mvvm.f4wzy.com.samplelogin.util.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



class LoginViewModel(application: Application) : AndroidViewModel(application), Callback<User> {


    var btnSelected: ObservableBoolean? = null
    var username: ObservableField<String>? = null
    var password: ObservableField<String>? = null
    var progressDialog: SingleLiveEvent<Boolean>? = null
    var userLogin: MutableLiveData<User>? = null

    init {
        btnSelected = ObservableBoolean(false)
        progressDialog = SingleLiveEvent<Boolean>()
        username = ObservableField("")
        password = ObservableField("")
        userLogin = MutableLiveData<User>()

    }

    fun onUsernameChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        btnSelected?.set( s.toString().length >= 8 && password?.get()!!.length >= 8)


    }

    fun onPasswordChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        btnSelected?.set(username?.get()!!.length >= 8 && s.toString().length >= 8)


    }

    fun login() {
        progressDialog?.value = true
                val userInfo = User(  username = username?.get()!!,
                password = password?.get()!!,
                id = 1
        )
        WebServiceClient.client.create(BackEndApi::class.java).LOGIN(userInfo)
                .enqueue(this)
    }

    override fun onResponse(call: Call<User>?, response: Response<User>?) {
        progressDialog?.value = false
        userLogin?.value = response?.body()

    }

    override fun onFailure(call: Call<User>?, t: Throwable?) {
        progressDialog?.value = false

    }

}