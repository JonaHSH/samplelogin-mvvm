package mvvm.f4wzy.com.samplelogin.model

data class AnimationModel(  val name: String,
                            val image: String) {
}